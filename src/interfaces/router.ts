// eslint-disable-next-line import/extensions,import/no-unresolved
import { Route } from 'vue-router/types/router';

export interface IRoute extends Partial<Route> {}

export interface ILink {
  title: string;
  to: IRoute;
}
