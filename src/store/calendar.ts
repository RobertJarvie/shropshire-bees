import { Module, VuexModule } from 'vuex-module-decorators';
import { Vue } from 'nuxt-property-decorator';
import { DateTime } from 'luxon';
import { ICalendarEvent, ICalendarRepeats } from '@/src/interfaces/calendar';

const colors = {
  apiary: 'amber darken-2',
  shows: 'blue-grey darken-2',
  training: 'blue',
  cancel: 'error',
};

function lastDayOfMonth(date: Date, day: number): boolean {
  const lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);

  if (lastDay.getDay() < day) {
    lastDay.setDate(lastDay.getDate() - 7);
  }
  lastDay.setDate(lastDay.getDate() - (lastDay.getDay() - day));

  return DateTime.fromJSDate(date).hasSame(DateTime.fromJSDate(lastDay), 'day');
}

@Module({
  stateFactory: true,
  namespaced: true,
})
export default class Calendar extends VuexModule {
  // The times in here are automatically shifted to the correct DST-status
  events: ICalendarEvent[] = [
    {
      name: 'Caradoc and Severn Valley Field Club',
      start: new Date('2024-07-10T10:00'),
      end: new Date('2024-07-10T12:00'),
    },
    {
      name: 'BBKA Module 3 Study Group',
      start: new Date('2023-11-15T18:30'),
      end: new Date('2023-11-15T20:30'),
      color: colors.training,
    },
    {
      name: 'Taster Day 2024',
      start: new Date('2024-09-14T10:00'),
      end: new Date('2024-09-14T16:00'),
      color: colors.training,
    },
  ];

  // Default to weekly unless otherwise stated
  repeatEvents: ICalendarRepeats[] = [
    {
      name: 'Apiary Open',
      color: colors.apiary,
      description:
        'Regular working session open to both members and visitors. Shop also open for honey and beekeeping supplies.',
      startTime: {
        hour: 9,
        minute: 30,
      },
      endTime: {
        hour: 12,
        minute: 30,
      },
      days: [3, 6],
      except: [new Date('2023-07-15')],
    },
    {
      name: 'Committee meeting',
      color: 'purple',
      startTime: {
        hour: 19,
        minute: 0,
      },
      endTime: {
        hour: 20,
        minute: 0,
      },
      repeat: 'LAST_TUESDAY_OF_MONTH',
      except: [],
    },
    {
      name: 'Beginners Course - Evenings',
      startDate: new Date('2023-07-26'),
      endDate: new Date('2023-08-30'),
      color: colors.training,
      startTime: {
        hour: 18,
        minute: 30,
      },
      endTime: {
        hour: 20,
        minute: 30,
      },
      days: [3],
    },
    {
      name: 'BBKA Basic Assessment',
      startDate: new Date('2024-05-15'),
      endDate: new Date('2024-06-19'),
      color: colors.training,
      startTime: {
        hour: 18,
        minute: 30,
      },
      endTime: {
        hour: 20,
        minute: 30,
      },
      days: [3],
    },
    {
      name: 'Beginners Course - Saturday',
      startDate: new Date('2024-06-01'),
      endDate: new Date('2024-06-15'),
      color: colors.training,
      startTime: {
        hour: 10,
        minute: 30,
      },
      endTime: {
        hour: 15,
        minute: 30,
      },
      days: [6],
    },
    {
      name: 'Beginners Course - Evenings',
      startDate: new Date('2024-07-17'),
      endDate: new Date('2024-08-21'),
      color: colors.training,
      startTime: {
        hour: 18,
        minute: 30,
      },
      endTime: {
        hour: 20,
        minute: 30,
      },
      days: [3],
    },
  ];

  static getOffset(date: Date): Date {
    const timestamp = date.getTime();
    return new Date(timestamp + date.getTimezoneOffset() * 60 * 1000);
  }

  /* eslint-disable prettier/prettier */
  get items() {
    return (min: Date, max: Date) => {
      const events : ICalendarEvent[] = [];

      events.push(
        ...this.events
          .filter((item) => {
            const eventStart = item.start.getTime();
            const eventEnd = item.end.getTime();
            const minTime = min.getTime();
            const maxTime = max.getTime();

            // This check ensure that multi-day events are captured
            const startInRange = eventStart <= maxTime && eventEnd >= minTime;
            const endIsInRange = eventEnd >= minTime && eventEnd <= maxTime;

            return startInRange || endIsInRange;
          })
          .map((item) => ({
            ...item,
            start: Calendar.getOffset(item.start),
            end: Calendar.getOffset(item.end),
          })),
      );

      /* Get all the repeated items */
      const repeats = this.repeatEvents
        .reduce((result: { [key: number]: ICalendarRepeats[]}, item: ICalendarRepeats) => {
          const days = [];

          if (item.days) {
            days.push(...item.days);
          } else if (item.repeat) {
            days.push(...[0, 1, 2, 3, 4, 5, 6]);
          }

          days.forEach((day) => {
            if (!(day in result)) {
              Vue.set(result, day, []);
            }

            result[day].push(item);
          });

          return result;
        }, {});

      const dayLength = 86400000;
      const days = Math.ceil((max.getTime() - min.getTime()) / dayLength);

      for (let i = 0; i < days; i += 1) {
        const day = new Date(min.getTime() + (dayLength * i));
        const date = DateTime.fromJSDate(day).toISODate();
        const luxonDate = DateTime.fromJSDate(day);

        const dayCode = day.getDay();

        if (dayCode in repeats) {
          repeats[dayCode].forEach((item) => {
            if (item.startDate && luxonDate.startOf('day') < DateTime.fromJSDate(item.startDate).startOf('day')) {
              return;
            }
            if (item.endDate && luxonDate.startOf('day') > DateTime.fromJSDate(item.endDate).startOf('day')) {
              return;
            }

            const excepted = (item.except ?? []).find((e: Date) => date === DateTime
              .fromJSDate(e)
              .toISODate());

            if (excepted) {
              return;
            }

            if (item.repeat && item.repeat === 'LAST_TUESDAY_OF_MONTH') {
              if (lastDayOfMonth(day, 2) === false) {
                return;
              }
            }

            events.push({
              ...item as unknown as ICalendarEvent,
              end: luxonDate.set({
                second: 0,
                millisecond: 0,
                ...item.endTime,
              }).toJSDate(),
              start: luxonDate.set({
                second: 0,
                millisecond: 0,
                ...item.startTime,
              }).toJSDate(),
            });
          });
        }
      }

      return events.map((item) => ({
        ...item,
        timed: item.timed !== false,
      })).sort((a: ICalendarEvent, b: ICalendarEvent) => {
        const aTime = a.start.getTime();
        const bTime = b.start.getTime();

        if (aTime < bTime) {
          return -1;
        }
        if (aTime > bTime) {
          return 1;
        }
        return 0;
      });
    };
  }
}
