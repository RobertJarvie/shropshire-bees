// eslint-disable-next-line object-curly-newline
import { Action, Module, Mutation, VuexModule } from 'vuex-module-decorators';
import sha256 from 'crypto-js/sha256';
import { IRoute } from '@/src/interfaces/router';

@Module({
  stateFactory: true,
  namespaced: true,
})
export default class Members extends VuexModule {
  pass: string = '';

  goToOnLogin?: IRoute;

  get password() {
    return this.pass;
  }

  get loginRoute() {
    return this.goToOnLogin;
  }

  @Mutation
  goTo(route?: IRoute) {
    this.goToOnLogin = route;
  }

  @Mutation
  setPassword(password: string) {
    this.pass = password;
  }

  @Action({ rawError: true })
  async login(password: string): Promise<boolean> {
    // First, unset the password
    this.context.commit('setPassword', '');

    if (password === '') {
      throw new Error('No password entered.');
    }

    try {
      // This is not cryptographically secure, but it's good enough for our purposes
      const membersPassword = process.env.MEMBERS_PASSWORD!;

      const [passwordHash, salt] = membersPassword.split(':');
      const hash = sha256(`${password}${salt}`).toString();

      if (hash === passwordHash) {
        this.context.commit('setPassword', password);
        return true;
      }
    } catch (err) {
      console.error(err);
      throw new Error('General error - please try again later.');
    }

    throw new Error('Invalid password.');
  }
}
