import { ServerResponse } from 'http';
import { Plugin } from '@nuxt/types'; // eslint-disable-line import/no-extraneous-dependencies
import createPersistedState from 'vuex-persistedstate';
import cookieUniversal from 'cookie-universal';
import { IncomingMessage } from 'connect';
import { CookieSerializeOptions } from 'cookie';

function factory(req: IncomingMessage, res: ServerResponse, setOpts: CookieSerializeOptions = {}) {
  const cookie = cookieUniversal(req, res);

  // @todo may have to set the domain - check once deployed
  const opts = {
    ...setOpts,
    path: '/',
    strict: true,
  };

  return {
    getItem: (key: string): any => cookie.get(key, { parseJSON: false }),
    // @todo as above, remove might need domain
    removeItem: (key: string): void => cookie.remove(key, { path: '/' }),
    setItem: (key: string, value: any): void => cookie.set(key, value, opts),
  };
}

// eslint-disable-next-line object-curly-newline
const plugin: Plugin = ({ req, res, store }): void => {
  createPersistedState({
    key: 'sbka',
    paths: ['members.pass'],
    storage: factory(req, res),
  })(store);
};

export default plugin;
