import { DateTime, DateTimeFormatOptions } from 'luxon';

export default (value: Date, fmt?: string) => {
  let format: Intl.DateTimeFormatOptions;
  if (fmt === 'MONTH_YEAR') {
    format = { year: 'numeric', month: 'long' };
  } else {
    format = fmt ? ((DateTime as any)[fmt] as DateTimeFormatOptions) : DateTime.DATE_FULL;
  }

  return DateTime.fromJSDate(value).toLocaleString(format);
};
