---
title: Committee & Trustees
subtitle: The committee and trustees are responsible for the Association
image: /img/pages/swarms/IMG_20210509_184934.jpg
imageLarge: true
order: 2
---

## Trustees

The trustees of Shropshire Beekeepers Association set the formal policy. They
are elected at the Annual General Meeting, serve for 3 years and 1/3 retire in
rotation.

<sbka-gallery md="4" :img="[{
  url: '/img/gallery/trustees/glynwilliams.jpg',
  caption: 'Glyn Williams, Chairman',
}, {
  url: '/img/gallery/trustees/marknewman.jpg',
  caption: 'Mark Newman, Vice Chairman',
}, {
  url: '/img/gallery/trustees/richardhudson.jpg',
  caption: 'Richard Hudson, Treasurer',
}, {
  url: '/img/gallery/trustees/johnconnolly.jpg',
  caption: 'John Connolly',
}, {
  url: '/img/gallery/trustees/anthonyrigby.jpg',
  caption: 'Sir Anthony Rigby',
}, {
  url: '/img/gallery/trustees/johnadams.jpg',
  caption: 'John Adams',
}]"></sbka-gallery>

## Committee

> New members are always welcome to join the committee. Speak to one
> of the existing committee members to talk about joining.

The committee is responsible for the day-to-day running. They are
appointed at the Annual General Meeting and meet monthly.

| Role | Name |
| --- | --- |
| President | Brian Goodwin |
| Chairman | Glyn Williams |
| Vice Chairman, Asian Hornet Action Team | Mark Newman |
| Secretary | Chris Currier |
| Treasurer and ADM Rep | Richard Hudson |
| Membership Secretary | Rob Jarvie |
| Apiary Manager | Ivor Huckin |
| Newsletter Editor | Matt Comerford |
| Fundraiser | David Draper |
| Library, Show Co-ordinator, Apiary Visit Co-ordinator | John Connolly |
| Minutes | Will Winton |
| Health and Safety Officer | Will Jones |
| Programme Organiser, Education | John Adams |
| | John Brown |
| | Rita Cliffe |
| Oswestry Representative | Sir Anthony Rigby |
