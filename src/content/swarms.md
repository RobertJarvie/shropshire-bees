---
title: Swarms
subtitle: A swarm of bees in your garden can be a stressful experience. We can help.
image: /img/pages/swarms/IMG_20210509_184934.jpg
imageLarge: true
displayToc: false
description: Beekeepers are often approached about winged, flying creatures, especially in the spring and summer period, when they are their most active simply working and are no cause for alarm. We can only help if they are honey bees.
---

<sbka-alert text color="red" border="left">
  <h6>What to do if you have a swarm</h6>

  <ul>
    <li>
      Stay at a safe distance.
    </li>
    <li>
      Ensure you have <a href="#identifying-a-swarm">correctly identified</a> that it is a swarm of honey
      bees. Most swarm collectors will ask for a clear photograph of the insects in question.
    </li>
    <li>
      Do not disturb, disperse, smoke or or in any way try to destroy the swarm. <strong>When in a
      swarm, honey bees are extremely unlikely to sting you.</strong>
    </li>
    <li>
      Contact a beekeeper using <a href="#find-a-swarm-collector">the map below</a>.
    </li>
  </ul>
</sbka-alert>

## What is a swarm?

A swarm is a natural process that honey bees undertake between about April and July. It is the first
part of how a colony reproduces where the old queen leaves with about half the bees. They leave
the original colony and find somewhere to cluster whilst the scout bees look for a place to set up a
permanent home. This can take anything from a few hours to a couple of days.

**This is the best time for a beekeeper to collect the swarm.**

If the bees are not collected, they may colonise a chimney, wall cavity or roof space which will
require specialist equipment to extract.

## Identifying a swarm

> The honey bee is the only species of bee that swarms. Our volunteer beekeepers are only able to
> assist in cases of swarms of honey bees.
>
> We cannot help with removal of wasps nests and will not remove nests of any other species of bee.

### Examples of swarms

A honey bee swarm is typically about the same size of a rugby ball and will contain many thousands of
bees. They will be located at any above-ground position - sometimes they will be just a few
centimetres high or can be located at the top of very tall trees. If you see bees in the ground, it's
highly unlikely that these are honey bees.

<sbka-gallery md="3" :img="[{
  url: '/img/gallery/swarms/bush.jpg',
  caption: 'Swarms can be hard to see',
}, {
  url: '/img/gallery/swarms/garden.jpg',
  caption: 'They can be obvious and accessible',
}, {
  url: '/img/gallery/swarms/tree.jpg',
  caption: 'Sometimes, they\'re really high up',
}, {
  url: '/img/gallery/swarms/shed.jpg',
  caption: 'And they can cling to anything',
}]"></sbka-gallery>

### Other species of bee

> We will not remove nests of any other species of bee. Please see the [Bumblebee Conservation
> Trust](https://www.bumblebeeconservation.org/bumblebee-nests) for more information.

There are around 250 species of bee in the UK. Of these, the only species that swarms is the honey
bee. These bees will often nest in bird boxes, cracks in mortar or in the ground. These bees are
important pollinators.

If you have bees nesting in your garden, please leave them alone. They are extremely unlikely to be
aggressive towards humans and will die out towards the end of summer. Rather than being fearful of
these wonderful insects, watch them, enjoy them and observe their behaviour.

**Moving these species of bee is likely to result in destruction of the nest.**

<sbka-gallery md="3" :img="[{
  url: '/img/gallery/swarms/bumblebee.jpg',
  caption: 'Buff tailed bumblebee',
}, {
  url: '/img/gallery/swarms/tawnyminingbee.jpg',
  caption: 'Tawny mining bee',
}, {
  url: '/img/gallery/swarms/ivyminingbee.jpg',
  caption: 'Ivy mining bee',
}]">
</sbka-gallery>

### Wasps and hornets

> We cannot help with removal of wasp or hornet nests.

Like honey bees, wasps and hornets are social insects that have large nests. Nests will often be
found in trees or the roof of your house or shed. Wasps have distinctive yellow and black bodies
and will be attracted to sweet things. Hornets are larger and have a loud buzz.

> The Asian hornet is an invasive species and a major threat to honey bees. If you believe you have
> found one, please report it via the Asian Hornet App available on
> [Apple](https://apps.apple.com/gb/app/asian-hornet-watch/id1161238813) or
> [Android](https://play.google.com/store/apps/details?id=uk.ac.ceh.hornets) or via the [BBKA Asian
> Hornet team](https://www.bbka.org.uk/asian-hornet-action-team-map).

<sbka-gallery md="3" :img="[{
  url: '/img/gallery/swarms/wasp.jpg',
  caption: 'Common wasp',
}, {
  url: '/img/gallery/swarms/hornet.jpg',
  caption: 'European hornet',
}, {
  url: '/img/gallery/swarms/asian-hornet.jpg',
  caption: 'Asian hornet',
}]">
</sbka-gallery>

## Find a swarm collector

Enter your postcode below and then click on the red markers which appear nearest the 'bee' for
details of your local swarm collector.

Please remember our beekeepers are volunteers and will have other commitments so may not be able
to attend your request immediately.

> We are only able to help with honey bees, not any other species of bee, wasp or hornet.
>
> However, we can help identify Asian Hornets.

<sbka-swarm-map></sbka-swarm-map>
