---
title: Join
subtitle: How to become a member of our association
image: /img/pages/index/bianca-ackermann-KlyZGkmE01w-unsplash.jpg
imageLarge: true
---

> <a href="/docs/2022-2023-membership.docx" target="_blank">Download our membership form</a>

## Membership benefits

<v-simple-table>
  <template v-slot:default>
    <thead>
      <tr>
        <th></th>
        <th>Associate</th>
        <th>Full/Partner</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td class="font-weight-bold">SBKA membership</td>
        <td class="text-center">
          <v-icon color="success">mdi-check</v-icon>
        </td>
        <td class="text-center">
          <v-icon color="success">mdi-check</v-icon>
        </td>
      </tr>
      <tr>
        <td class="font-weight-bold">BBKA membership</td>
        <td class="text-center">
          <v-icon color="error">mdi-close</v-icon>
        </td>
        <td class="text-center">
          <v-icon color="success">mdi-check</v-icon>
        </td>
      </tr>
      <tr>
        <td class="font-weight-bold">Bee disease insurance</td>
        <td class="text-center">
          <v-icon color="error">mdi-close</v-icon>
        </td>
        <td class="text-center">
          <v-icon color="success">mdi-check</v-icon>
        </td>
      </tr>
      <tr>
        <td class="font-weight-bold">Annual cost</td>
        <td class="text-center">
          &pound;12.00
        </td>
        <td class="text-center">
          &pound;33.00/&pound;19.00
        </td>
      </tr>
    </tbody>
  </template>
</v-simple-table>

To qualify for partner membership, you must living at the same address as
a full member.

Membership runs from 1st October to 30th September each year. Renewals must
be received by the end of November in order to ensure your insurance cover
continues uninterrupted.

### Bee disease insurance

- 1 to 3 colonies - included in full membership
- 4 to 5 colonies - an additional £1.20
- 6 to 10 colonies - an additional £4.70
- 11 to 15 colonies - an additional £7.75
- 16 to 20 colonies - an additional £9.50
- 21 to 25 colonies - an additional £11.10
- 26 to 30 colonies - an additional £13.60
- 31 to 35 colonies - an additional £16.10
- 36 to 39 colonies - an additional £18.10

To find out more about BDI Ltd please visit [their website](https://www.beediseasesinsurance.co.uk/)
